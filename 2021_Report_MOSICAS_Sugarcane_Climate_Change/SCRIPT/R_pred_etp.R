#### test météo

FILES <- list.files('input/Weather and irrigation data/')[-6]
ALT <- c(113,257,798,15,147,652,68,862,158,182,20,147,45,15,179)

MET <- read.table(paste('input/Weather and irrigation data/',FILES[1],sep=''),header=T)
MET$ALT <- rep(ALT[1],dim(MET)[1])
MET$STA <- rep(1,dim(MET)[1])

for (i in 2:length(FILES)){
  MTEMP<- read.table(paste('input/Weather and irrigation data/',FILES[i],sep=''),header=T)
  MTEMP$ALT <- rep(ALT[i],dim(MTEMP)[1])
  MTEMP$STA <- rep(i,dim(MTEMP)[1])
  MET <- rbind(MET,MTEMP)
}

plot(MET$tx,MET$etp)

YY <- lm(MET$etp ~ MET$tm*MET$tx*MET$tn)
YY <- lm(MET$etp ~ MET$tm*MET$tx*MET$tn*MET$rg)
YY <- lm(MET$etp ~ MET$tm*MET$rr)


YY <- lm(MET$etp ~ MET$tm*MET$rg*MET$ALT)

plot(MET$etp, predict(YY))
plot(1)

XOBS <- c()
XSIM <- c()
par(mfrow=c(3,5))
for (i in 1:15){

  YY <- lm(etp ~ tm*rg*tx*tn,data=subset(MET,MET$STA==i))
#  YY <- lm(etp ~ tm*rg,data=subset(MET,MET$STA==i))0
  plot(MET$etp[MET$STA==i],predict(YY))
  abline(0,1,col=2)
  XOBS <- c(XOBS,MET$etp[MET$STA==i])
  XSIM <- c(XSIM,as.vector(predict(YY)))
  
  
}

RMSE <- sqrt(sum((XSIM-XOBS)^2)/(length(XSIM)-4-1))

