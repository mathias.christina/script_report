library(readxl)
library(stringr)
library(dplyr)

########## DATA PREP ########
MATMET <- read.csv2('DATA/TABLE_RU_LAT_LONG.csv',header=T,stringsAsFactors = F)
head(MATMET)

AREA <- read_xlsx('DATA/2002_2020_surface-canne_maille-BRIO.xlsx',sheet = 2,col_names = T)
AREA$MEAN <- rowMeans(AREA[,22:26],na.rm=T)
AREA$MEAN[which(is.na(AREA$MEAN))] <- 0

names(MATMET)[1] <- 'id_maille'
MATMET <- merge(MATMET, AREA[,c("id_maille","MEAN")], by='id_maille',all.x=T,all.y=F)
MATMET$MEAN[which(is.na(MATMET$MEAN))] <- 0

LIST <- list.files('input/Weather and irrigation data/')[1:300]

YEAR <- 2015:2100


SCE <- c('ssp126','ssp245','ssp585')
ZONE <- c('RUN',unique(MATMET$p_labelzon))

listALLMET <- list()
for (isce in 1:3){
  listweath <- list()
  for (izone in 1:length(ZONE)){

    ID1 <- which(str_detect(LIST,SCE[isce]))
    if(ZONE[izone]=='RUN'){
      ID2 <- 1:length(LIST)      
      listmet <- MATMET$id_maille
    }
    if(ZONE[izone]!='RUN'){
      listmet <- as.character(MATMET$id_maille[MATMET$p_labelzon==ZONE[izone]])
      ID2 <- as.vector(sapply(1:length(listmet), function(jj) which(str_detect(LIST,listmet[jj]))))}
    listtemp <- LIST[intersect(ID1,ID2)]
    
    areatemp <- MATMET$MEAN[which(MATMET$id_maille %in% as.numeric(listmet))]
    
    for (imet in 1:length(listtemp)){
      WTEMPM <- read.table(paste0('input/Weather and irrigation data/',
                                  listtemp[imet]),header=T)
      WTEMPM$year <- format(as.Date(WTEMPM$date,format='%d-%m-%Y'),'%Y')
      RFintense <- sapply(2015:2100, function(ii) length(which(WTEMPM$rr[WTEMPM$year==ii]>28.8)))
      
      WTEMPM <- WTEMPM[,2:12] %>% group_by(year) %>% summarise_all(mean)
      WTEMPM$rr <- WTEMPM$rr*365
      WTEMPM$RFintense <- RFintense  
      
      if(imet==1){listweath[[izone]] <- WTEMPM[,2:11]*areatemp[imet]/sum(areatemp)}
      if(imet>1){
        listweath[[izone]] <- listweath[[izone]] + WTEMPM[,2:11]*areatemp[imet]/sum(areatemp)}
    }
    print(izone)
  }
  names(listweath) <- ZONE
  listALLMET[[isce]] <- listweath
}
names(listALLMET) <- SCE

save.image('DATA/R_sauv_meteo')

###### figure ###########
load('DATA/R_sauv_meteo')

x11()

mycols1 <- adjustcolor(palette('Default'), alpha.f = 0.05)
mycols2 <- adjustcolor(palette('Default'), alpha.f = 0.2)

par(mfrow=c(4,3),mar=c(3,3,1,1))

for (isce in 1:3){
  xtemp <- 2015:2100
  ytemp <- listALLMET[[isce]]$RUN$tm
  fit <- lm(ytemp~xtemp);coef <- round(coef(fit)[2],2)
  fitloess= loess(ytemp ~ xtemp, family="gaussian", span=.6, degree=1)
  new.x = seq(min(xtemp),max(xtemp), length.out=200)
  ypred <- predict(fitloess, data.frame(xtemp=new.x),se=T)
  ci= cbind(ypred$fit,ypred$fit+ypred$se.fit*qnorm(1-.05/2),
            ypred$fit-ypred$se.fit*qnorm(1-.05/2))

  plot(xtemp,ytemp,type='p',ylim=c(22,28),pch=21,col=1,bg='white',cex=1.5,bty='n',mgp=c(1.8,0.5,0),
       xlab='year',ylab='Mean temperature (°C)',cex.lab=1.2)
  points(new.x,ci[,1],type='l',lwd=2,col=1)
  polygon(c(new.x,rev(new.x)),c(ci[,2],rev(ci[,1])),border=NA,col=mycols2[1])
  polygon(c(new.x,rev(new.x)),c(ci[,1],rev(ci[,3])),border=NA,col=mycols2[1])
  
  legend('topleft',legend=paste0('+ ',coef,' °C/yr'),cex=1.2,bty='n')
  legend('topright',legend=SCE[isce],cex=1.2,bty='n')
}

for (isce in 1:3){
  xtemp <- 2015:2100
  ytemp <- listALLMET[[isce]]$RUN$rr
  fit <- lm(ytemp~xtemp);coef <- round(coef(fit)[2],2)
  fitloess= loess(ytemp ~ xtemp, family="gaussian", span=.6, degree=1)
  new.x = seq(min(xtemp),max(xtemp), length.out=200)
  ypred <- predict(fitloess, data.frame(xtemp=new.x),se=T)
  ci= cbind(ypred$fit,ypred$fit+ypred$se.fit*qnorm(1-.05/2),
            ypred$fit-ypred$se.fit*qnorm(1-.05/2))
  plot(xtemp,ytemp,type='p',pch=21,col=1,bg='white',cex=1.5,bty='n',mgp=c(1.8,0.5,0),
       xlab='year',ylab='Annual rainfall (mm/yr)',cex.lab=1.2,ylim=c(0,6000))
  points(new.x,ci[,1],type='l',lwd=2,col=1)
  polygon(c(new.x,rev(new.x)),c(ci[,2],rev(ci[,1])),border=NA,col=mycols2[1])
  polygon(c(new.x,rev(new.x)),c(ci[,1],rev(ci[,3])),border=NA,col=mycols2[1])
}

for (isce in 1:3){
  xtemp <- 2015:2100
  ytemp <- listALLMET[[isce]]$RUN$etp
  fit <- lm(ytemp~xtemp);coef <- round(coef(fit)[2],3)
  fitloess= loess(ytemp ~ xtemp, family="gaussian", span=.6, degree=1)
  new.x = seq(min(xtemp),max(xtemp), length.out=200)
  ypred <- predict(fitloess, data.frame(xtemp=new.x),se=T)
  ci= cbind(ypred$fit,ypred$fit+ypred$se.fit*qnorm(1-.05/2),
            ypred$fit-ypred$se.fit*qnorm(1-.05/2))
  plot(xtemp,ytemp,type='p',pch=21,col=1,bg='white',cex=1.5,bty='n',mgp=c(1.8,0.5,0),
       xlab='year',ylab='Mean ETP (mm/d)',cex.lab=1.2,ylim=c(3.6,4.5))
  points(new.x,ci[,1],type='l',lwd=2,col=1)
  polygon(c(new.x,rev(new.x)),c(ci[,2],rev(ci[,1])),border=NA,col=mycols2[1])
  polygon(c(new.x,rev(new.x)),c(ci[,1],rev(ci[,3])),border=NA,col=mycols2[1])
  legend('topleft',legend=paste0('+ ',coef,' mm/yr'),cex=1.2,bty='n')
}

for (isce in 1:3){
  xtemp <- 2015:2100
  ytemp <- listALLMET[[isce]]$RUN$rg
  fit <- lm(ytemp~xtemp);coef <- round(coef(fit)[2],3)
  fitloess= loess(ytemp ~ xtemp, family="gaussian", span=.6, degree=1)
  new.x = seq(min(xtemp),max(xtemp), length.out=200)
  ypred <- predict(fitloess, data.frame(xtemp=new.x),se=T)
  ci= cbind(ypred$fit,ypred$fit+ypred$se.fit*qnorm(1-.05/2),
            ypred$fit-ypred$se.fit*qnorm(1-.05/2))
  plot(xtemp,ytemp,type='p',pch=21,col=1,bg='white',cex=1.5,bty='n',mgp=c(1.8,0.5,0),
       xlab='year',ylab='Mean Global radiation (MJ/m²/d)',cex.lab=1.2,ylim=c(15,20))
  points(new.x,ci[,1],type='l',lwd=2,col=1)
  polygon(c(new.x,rev(new.x)),c(ci[,2],rev(ci[,1])),border=NA,col=mycols2[1])
  polygon(c(new.x,rev(new.x)),c(ci[,1],rev(ci[,3])),border=NA,col=mycols2[1])
  #  legend('topleft',legend=paste0('+ ',coef,'mm/yr'),cex=1.2,bty='n')
}

###### Evolution annuelle ####
YEAR <- 2015:2100
SCE <- c('ssp126','ssp245','ssp585')

list_maille_MET <- list()
for (isce in 1:3){
  listmaille <- list()
  for (imet in 1:100){
    FILE <- paste0(MATMET$id_maille[imet],'_',SCE[isce],'.txt')
    WTEMPM <- read.table(paste0('input/Weather and irrigation data/',
                                  FILE),header=T)
    WTEMPM$year <- format(as.Date(WTEMPM$date,format='%d-%m-%Y'),'%Y')
    WTEMPM$month <- format(as.Date(WTEMPM$date,format='%d-%m-%Y'),'%m')
    listmaille[[imet]] <- WTEMPM
    print(imet)
  }
  names(listmaille) <- MATMET$id_maille
  list_maille_MET[[isce]] <- listmaille
}
names(list_maille_MET) <- SCE

save.image('DATA/R_sauv_meteo')

##### evolution annuelle #####
load('DATA/R_sauv_meteo')

list_maille_MONTH <- list()

for (isce in 1:3){
  listmaille <- list()
  for (imet in 1:100){
    WTEMPM <- list_maille_MET[[isce]][[imet]]
    
    ID <- which(list_maille_MET[[isce]][[imet]]$year %in% 2015:2024)
    WTEMPM1 <- WTEMPM[ID,c(2:13)] %>% group_by(year,month) %>% summarise_all(mean)
    WTEMPM1$rr <- (WTEMPM[ID,c(2:13)] %>% group_by(year,month) %>% summarise_all(sum))$rr
    TMEAN1 <- sapply(unique(WTEMPM1$month), function(i) mean(WTEMPM1$tm[WTEMPM1$month==i]))
    sdTMEAN1 <- sapply(unique(WTEMPM1$month), function(i) mean(WTEMPM1$tm[WTEMPM1$month==i]))
    PPT1 <- sapply(unique(WTEMPM1$month), function(i) mean(WTEMPM1$rr[WTEMPM1$month==i]))
    sdPPT1 <- sapply(unique(WTEMPM1$month), function(i) mean(WTEMPM1$rr[WTEMPM1$month==i]))
    ETP1 <- sapply(unique(WTEMPM1$month), function(i) mean(WTEMPM1$etp[WTEMPM1$month==i]))
    sdETP1 <- sapply(unique(WTEMPM1$month), function(i) mean(WTEMPM1$etp[WTEMPM1$month==i]))
    
    ID <- which(list_maille_MET[[isce]][[imet]]$year %in% 2046:2055)
    WTEMPM2 <- WTEMPM[ID,c(2:13)] %>% group_by(year,month) %>% summarise_all(mean)
    WTEMPM2$rr <- (WTEMPM[ID,c(2:13)] %>% group_by(year,month) %>% summarise_all(sum))$rr
    TMEAN2 <- sapply(unique(WTEMPM2$month), function(i) mean(WTEMPM2$tm[WTEMPM2$month==i]))
    sdTMEAN2 <- sapply(unique(WTEMPM2$month), function(i) mean(WTEMPM2$tm[WTEMPM2$month==i]))
    PPT2 <- sapply(unique(WTEMPM2$month), function(i) mean(WTEMPM2$rr[WTEMPM2$month==i]))
    sdPPT2 <- sapply(unique(WTEMPM2$month), function(i) mean(WTEMPM2$rr[WTEMPM2$month==i]))
    ETP2 <- sapply(unique(WTEMPM2$month), function(i) mean(WTEMPM2$etp[WTEMPM2$month==i]))
    sdETP2 <- sapply(unique(WTEMPM2$month), function(i) mean(WTEMPM2$etp[WTEMPM2$month==i]))
    
    ID <- which(list_maille_MET[[isce]][[imet]]$year %in% 2091:2100)
    WTEMPM3 <- WTEMPM[ID,c(2:13)] %>% group_by(year,month) %>% summarise_all(mean)
    WTEMPM3$rr <- (WTEMPM[ID,c(2:13)] %>% group_by(year,month) %>% summarise_all(sum))$rr
    TMEAN3 <- sapply(unique(WTEMPM3$month), function(i) mean(WTEMPM3$tm[WTEMPM3$month==i]))
    sdTMEAN3 <- sapply(unique(WTEMPM3$month), function(i) mean(WTEMPM3$tm[WTEMPM3$month==i]))
    PPT3 <- sapply(unique(WTEMPM3$month), function(i) mean(WTEMPM3$rr[WTEMPM3$month==i]))
    sdPPT3 <- sapply(unique(WTEMPM3$month), function(i) mean(WTEMPM3$rr[WTEMPM3$month==i]))
    ETP3 <- sapply(unique(WTEMPM3$month), function(i) mean(WTEMPM3$etp[WTEMPM3$month==i]))
    sdETP3 <- sapply(unique(WTEMPM3$month), function(i) mean(WTEMPM3$etp[WTEMPM3$month==i]))
    
    listmaille[[imet]] <- data.frame(TMEAN1,TMEAN2,TMEAN3,
                                     PPT1,PPT2,PPT3,
                                     ETP1,ETP2,ETP3,
                                     sdTMEAN1,sdTMEAN2,sdTMEAN3,
                                     sdPPT1,sdPPT2,sdPPT3,
                                     sdETP1,sdETP2,sdETP3)
    print(imet)
  }
  names(listmaille) <- MATMET$id_maille
  list_maille_MONTH[[isce]] <- listmaille
}
names(list_maille_MONTH) <- SCE

ZONE <- c('RUN',unique(MATMET$p_labelzon))

list_maille_MONTH_ZONE <- c()
for (isce in 1:3){
  list_zone <- list()
  for (izone in 1:length(ZONE)){
    if(izone==1){ID <- 1:100}
    if(izone>1){
      listmet <- as.character(MATMET$id_maille[MATMET$p_labelzon==ZONE[izone]])
      ID <- which(MATMET$id_maille %in% listmet)
    }
    
    list_zone[[izone]] <- list_maille_MONTH[[isce]][[ID[1]]]*MATMET$MEAN[ID[1]]/sum(MATMET$MEAN[ID])
    if(length(ID)>1){
    for(ii in 2:length(ID)){
      list_zone[[izone]] <- list_zone[[izone]] +
        list_maille_MONTH[[isce]][[ID[ii]]]*MATMET$MEAN[ID[ii]]/sum(MATMET$MEAN[ID])
    }}
  }
  names(list_zone) <- ZONE
  list_maille_MONTH_ZONE[[isce]] <- list_zone
}
names(list_maille_MONTH_ZONE) <- SCE

plot(1:12,list_maille_MONTH_ZONE[[3]][[1]]$TMEAN1,ylim=c(19,30))
points(1:12,list_maille_MONTH_ZONE[[3]][[1]]$TMEAN2)
points(1:12,list_maille_MONTH_ZONE[[3]][[1]]$TMEAN3)

plot(1:12,list_maille_MONTH_ZONE[[3]][[1]]$PPT1,ylim=c(0,1000))
points(1:12,list_maille_MONTH_ZONE[[3]][[1]]$PPT2)
points(1:12,list_maille_MONTH_ZONE[[3]][[1]]$PPT3)

###### effet altitude ########

