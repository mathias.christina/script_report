library(ggplot2)
library(ggpubr)
library(dplyr)
library(readxl)
library(stringi)

######## data prep #########
LISTF <- list.files('output/SIM_FUTURE_R570_0123_ruini75/')
AREA <- read_xlsx(path = 'DATA/2002_2020_surface-canne_maille-BRIO.xlsx',sheet=2,col_names = T)
ZONE <- read.csv('DATA/TABLE_RU_LAT_LONG.csv',header=T,stringsAsFactors = F,sep=';',dec=',')
ZONE$Latitude <- as.numeric(ZONE$Latitude)
ZONE$Longitude <- as.numeric(ZONE$Longitude)

METNOSTRESS <- list()
METIRR0 <- list()
METIRR15 <- list()
METIRR35 <- list()
METIRR80 <- list()
IDnostress <- grep('nostress',LISTF)
IDIRR0 <- grep('IRR0',LISTF)
IDIRR15 <- grep('IRR15',LISTF)
IDIRR35 <- grep('IRR35',LISTF)
IDIRR80 <- grep('IRR80',LISTF)
for (i in 1:length(IDnostress)){
  METNOSTRESS[[i]] <- read.csv(paste0('output/SIM_FUTURE_R570_0123_ruini75/',LISTF[IDnostress[i]]),
                               header=T,stringsAsFactors = F,sep=';')
  METIRR0[[i]] <- read.csv(paste0('output/SIM_FUTURE_R570_0123_ruini75/',LISTF[IDIRR0[i]]),
                               header=T,stringsAsFactors = F,sep=';')
  METIRR15[[i]] <- read.csv(paste0('output/SIM_FUTURE_R570_0123_ruini75/',LISTF[IDIRR15[i]]),
                            header=T,stringsAsFactors = F,sep=';')
  METIRR35[[i]] <- read.csv(paste0('output/SIM_FUTURE_R570_0123_ruini75/',LISTF[IDIRR35[i]]),
                               header=T,stringsAsFactors = F,sep=';')
  METIRR80[[i]] <- read.csv(paste0('output/SIM_FUTURE_R570_0123_ruini75/',LISTF[IDIRR80[i]]),
                               header=T,stringsAsFactors = F,sep=';')
  print(i)
}

METNOSTRESS <- do.call("bind_rows", METNOSTRESS)
METIRR0 <- do.call("bind_rows", METIRR0)
METIRR15 <- do.call("bind_rows", METIRR15)
METIRR35 <- do.call("bind_rows", METIRR35)
METIRR80 <- do.call("bind_rows", METIRR80)

head(METNOSTRESS)
METNOSTRESS$id <- substr(METNOSTRESS$treatcode,1,3)
METIRR0$id <- substr(METIRR0$treatcode,1,3)
METIRR15$id <- substr(METIRR15$treatcode,1,3)
METIRR35$id <- substr(METIRR35$treatcode,1,3)
METIRR80$id <- substr(METIRR80$treatcode,1,3)
METNOSTRESS$SCE <- substr(METNOSTRESS$treatcode,5,10)
METIRR0$SCE <- substr(METIRR0$treatcode,5,10)
METIRR15$SCE <- substr(METIRR15$treatcode,5,10)
METIRR35$SCE <- substr(METIRR35$treatcode,5,10)
METIRR80$SCE <- substr(METIRR80$treatcode,5,10)
METNOSTRESS$SEASON <- substr(METNOSTRESS$treatcode,12,13)
METIRR0$SEASON <- substr(METIRR0$treatcode,12,13)
METIRR15$SEASON <- substr(METIRR15$treatcode,12,13)
METIRR35$SEASON <- substr(METIRR35$treatcode,12,13)
METIRR80$SEASON <- substr(METIRR80$treatcode,12,13)
METNOSTRESS$YEAR <- as.numeric(substr(METNOSTRESS$treatcode,15,18))
METIRR0$YEAR <- as.numeric(substr(METIRR0$treatcode,15,18))
METIRR15$YEAR <- as.numeric(substr(METIRR15$treatcode,15,18))
METIRR35$YEAR <- as.numeric(substr(METIRR35$treatcode,15,18))
METIRR80$YEAR <- as.numeric(substr(METIRR80$treatcode,15,18))

head(AREA)
AREA <- replace(AREA,is.na(AREA),0)
names(AREA)[1] <- 'id'
AREA$Area <- (AREA$`2018_RPG`+AREA$`2019_RPG`+AREA$`2020_RPG`)/3

METNOSTRESS <- merge(METNOSTRESS,AREA[,c('id','Area')],by='id',all.x=T)
METIRR0 <- merge(METIRR0,AREA[,c('id','Area')],by='id',all.x=T)
METIRR15 <- merge(METIRR15,AREA[,c('id','Area')],by='id',all.x=T)
METIRR35 <- merge(METIRR35,AREA[,c('id','Area')],by='id',all.x=T)
METIRR80 <- merge(METIRR80,AREA[,c('id','Area')],by='id',all.x=T)

head(ZONE)

METNOSTRESS <- merge(METNOSTRESS,ZONE[,c('id','bc','p_ru','Irrigation','p_labelzon','libelle')],by='id',all.x=T)
METIRR0 <- merge(METIRR0,ZONE[,c('id','bc','p_ru','Irrigation','p_labelzon','libelle')],by='id',all.x=T)
METIRR15 <- merge(METIRR15,ZONE[,c('id','bc','p_ru','Irrigation','p_labelzon','libelle')],by='id',all.x=T)
METIRR35 <- merge(METIRR35,ZONE[,c('id','bc','p_ru','Irrigation','p_labelzon','libelle')],by='id',all.x=T)
METIRR80 <- merge(METIRR80,ZONE[,c('id','bc','p_ru','Irrigation','p_labelzon','libelle')],by='id',all.x=T)

METNOSTRESS$Area[which(is.na(METNOSTRESS$Area))] <- 0
METIRR0$Area[which(is.na(METIRR0$Area))] <- 0
METIRR15$Area[which(is.na(METIRR15$Area))] <- 0
METIRR35$Area[which(is.na(METIRR35$Area))] <- 0
METIRR80$Area[which(is.na(METIRR80$Area))] <- 0

head(METNOSTRESS)

METREAL <- rbind(METIRR0[which(METIRR0$Irrigation=='NO'),],METIRR15[which(METIRR15$Irrigation=='YES'),])
############ tableau climat prep #######
AREATOT <- sum(AREA$Area)

MRUN <- METNOSTRESS %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),
            ETP = sum(setp*Area/AREATOT))
MRUN <- MRUN %>% group_by(YEAR,SCE) %>% summarise_all(mean)

MZONE <- METNOSTRESS %>%
  group_by(p_labelzon,YEAR,SCE,SEASON) %>%
  summarise(TMEAN=sum(stmean/365*Area/sum(Area)),
            PPT=sum(spr*Area/sum(Area)),SDRAIN=sum(sdrain*Area/sum(Area)),
            ETP = sum(setp*Area/sum(Area)))
MZONE <- MZONE %>% group_by(p_labelzon,YEAR,SCE) %>% summarise_all(mean)

MRUN$p_labelzon <- 'Reunion'

MCLIM <- rbind(MRUN, MZONE)

LISTMEAN <-list()
IPERIOD <- list(c(2016:2025),c(2026:2035),c(2036:2045),c(2046:2035),c(2091:2100))
for (i in 1:length(IPERIOD)){
  LISTMEAN[[i]] <- MCLIM %>% group_by(p_labelzon,SCE) %>%
    filter(YEAR %in% IPERIOD[[i]]) %>%
    summarise_all(mean)
}
MATOUT <- LISTMEAN[[1]][,1:2]
for (i in c(5,6,8)){
MATOUT <- data.frame(MATOUT,
                     LISTMEAN[[2]][,i]-LISTMEAN[[1]][,i],
                     LISTMEAN[[3]][,i]-LISTMEAN[[1]][,i],
                     LISTMEAN[[4]][,i]-LISTMEAN[[1]][,i],
                     LISTMEAN[[5]][,i]-LISTMEAN[[1]][,i])
}
MATOUT[,3:6] <- round(MATOUT[,3:6],1)
MATOUT[,7:10] <- round(MATOUT[,7:10],0)
MATOUT[,11:14] <- round(MATOUT[,11:14],0)

write.table(MATOUT,'output/MEANCLIM.csv',quote = F,row.names=F,col.names = T,sep=';')

head(MATOUT)

############ tableau output prep #######
AREATOT <- sum(AREA$Area)

MRUN <- METREAL %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(STEMFM=sum(stemfm*Area/AREATOT))
MRUN <- MRUN %>% group_by(YEAR,SCE) %>% summarise_all(mean)
MRUN2 <- METIRR15 %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(STEMFM=sum(stemfm*Area/AREATOT))
MRUN2 <- MRUN2 %>% group_by(YEAR,SCE) %>% summarise_all(mean)

MZONE <- METREAL %>%
  group_by(p_labelzon,YEAR,SCE,SEASON) %>%
  summarise(STEMFM=sum(stemfm*Area/sum(Area)))
MZONE <- MZONE %>% group_by(p_labelzon,YEAR,SCE) %>% summarise_all(mean)
MZONE2 <- METIRR15 %>%
  group_by(p_labelzon,YEAR,SCE,SEASON) %>%
  summarise(STEMFM=sum(stemfm*Area/sum(Area)))
MZONE2 <- MZONE2 %>% group_by(p_labelzon,YEAR,SCE) %>% summarise_all(mean)

MRUN$p_labelzon <- 'Reunion'
MRUN2$p_labelzon <- 'Reunion'

MCLIM <- rbind(MRUN, MZONE)
MCLIM2 <- rbind(MRUN2, MZONE2)

LISTMEAN <-list();LISTMEAN2 <-list()
IPERIOD <- list(c(2016:2025),c(2026:2035),c(2036:2045),c(2046:2035),c(2091:2100))
for (i in 1:length(IPERIOD)){
  LISTMEAN[[i]] <- MCLIM %>% group_by(p_labelzon,SCE) %>%
    filter(YEAR %in% IPERIOD[[i]]) %>%
    summarise_all(mean)
  LISTMEAN2[[i]] <- MCLIM2 %>% group_by(p_labelzon,SCE) %>%
    filter(YEAR %in% IPERIOD[[i]]) %>%
    summarise_all(mean)
}
MATOUT <- LISTMEAN[[1]][,1:2]
for (i in c(5)){
  MATOUT <- data.frame(MATOUT,LISTMEAN[[1]][,i],
                       (LISTMEAN[[2]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       (LISTMEAN[[3]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       (LISTMEAN[[4]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       (LISTMEAN[[5]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       LISTMEAN2[[1]][,i],
                       (LISTMEAN2[[2]][,i]-LISTMEAN2[[1]][,i])/LISTMEAN2[[1]][,i]*100,
                       (LISTMEAN2[[3]][,i]-LISTMEAN2[[1]][,i])/LISTMEAN2[[1]][,i]*100,
                       (LISTMEAN2[[4]][,i]-LISTMEAN2[[1]][,i])/LISTMEAN2[[1]][,i]*100,
                       (LISTMEAN2[[5]][,i]-LISTMEAN2[[1]][,i])/LISTMEAN2[[1]][,i]*100)
}
MATOUT[,3:12] <- round(MATOUT[,3:12],0)

write.table(MATOUT,'output/MEANRDT.csv',quote = F,row.names=F,col.names = T,sep=';')

head(MATOUT)

############ tableau sirr prep #######
AREATOT <- sum(AREA$Area)

MRUN <- METIRR80 %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(SIRR=sum(sirr*Area/AREATOT),SIRRM3=sum(SIRR*Area)/1000/10^6)
MRUN <- MRUN %>% group_by(YEAR,SCE) %>% summarise_all(mean)

MZONE <- METIRR80 %>%
  group_by(p_labelzon,YEAR,SCE,SEASON) %>%
  summarise(SIRR=sum(sirr*Area/sum(Area)),SIRRM3=sum(SIRR*Area)/1000/10^6)
MZONE <- MZONE %>% group_by(p_labelzon,YEAR,SCE) %>% summarise_all(mean)

MRUN$p_labelzon <- 'Reunion'

MCLIM <- rbind(MRUN, MZONE)

LISTMEAN <-list();LISTMEAN2 <-list()
IPERIOD <- list(c(2016:2025),c(2026:2035),c(2036:2045),c(2046:2035),c(2091:2100))
for (i in 1:length(IPERIOD)){
  LISTMEAN[[i]] <- MCLIM %>% group_by(p_labelzon,SCE) %>%
    filter(YEAR %in% IPERIOD[[i]]) %>%
    summarise_all(mean)
}
MATOUT <- LISTMEAN[[1]][,1:2]
for (i in c(5)){
  MATOUT <- data.frame(MATOUT,LISTMEAN[[1]][,i],
                       (LISTMEAN[[2]][,i]-LISTMEAN[[1]][,i]),
                       (LISTMEAN[[3]][,i]-LISTMEAN[[1]][,i]),
                       (LISTMEAN[[4]][,i]-LISTMEAN[[1]][,i]),
                       (LISTMEAN[[5]][,i]-LISTMEAN[[1]][,i]),
                       (LISTMEAN[[2]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       (LISTMEAN[[3]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       (LISTMEAN[[4]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       (LISTMEAN[[5]][,i]-LISTMEAN[[1]][,i])/LISTMEAN[[1]][,i]*100,
                       LISTMEAN[[1]][,6],LISTMEAN[[2]][,6],LISTMEAN[[3]][,6],
                       LISTMEAN[[4]][,6],LISTMEAN[[5]][,6])
}
MATOUT[,3:11] <- round(MATOUT[,3:11],0)
MATOUT[,12:15] <- round(MATOUT[,12:15],1)

write.table(MATOUT,'output/MEANSIRR.csv',quote = F,row.names=F,col.names = T,sep=';')

head(MATOUT)

######## analyses stats préliminair #########
fit1 <- lm(stemfm~(YEAR+SEASON+SCE)^2,data=METREAL)
#fit2 <- lm(stemfm~(YEAR+SEASON+id)^2,data=subset(METIRR0,METIRR0$SCE=='ssp245'))
#fit3 <- lm(stemfm~(YEAR+SEASON+id)^2,data=subset(METIRR0,METIRR0$SCE=='ssp585'))
ggqqplot(fit1$residuals)
anova(fit1)
#anova(fit2)
#anova(fit3)
MM <- METREAL %>% group_by(SEASON) %>% filter(YEAR %in% c(2016:2025)) %>% 
  summarise(RDT = sum(stemfm*Area)/sum(Area))

#fit1 <- lm(stemfm~(YEAR+SEASON+id)^2,data=subset(METIRR15,METIRR15$SCE=='ssp126'))
#fit2 <- lm(stemfm~(YEAR+SEASON+id)^2,data=subset(METIRR15,METIRR15$SCE=='ssp245'))
#fit3 <- lm(stemfm~(YEAR+SEASON+id)^2,data=subset(METIRR15,METIRR15$SCE=='ssp585'))
#ggqqplot(fit3$residuals)
#anova(fit1)
#anova(fit2)
#anova(fit3)

##### Reunion Islande scale #########
AREATOT <- sum(AREA$Area)

MNOSTRESS <- METNOSTRESS %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(YIELD=sum(stemfm*Area/AREATOT),
            SIRR=sum(sirr*Area/AREATOT),TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),
            ETR = sum(setr*Area/AREATOT),ETP = sum(setp*Area/AREATOT))
MIRR0 <- METIRR0 %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(YIELD=sum(stemfm*Area/AREATOT),
            SIRR=sum(sirr*Area/AREATOT),TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),
            ETR = sum(setr*Area/AREATOT),ETP = sum(setp*Area/AREATOT))
MIRR15 <- METIRR15 %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(YIELD=sum(stemfm*Area/AREATOT),
            SIRR=sum(sirr*Area/AREATOT),TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),
            ETR = sum(setr*Area/AREATOT),ETP = sum(setp*Area/AREATOT))
MIRR35 <- METIRR35 %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(YIELD=sum(stemfm*Area/AREATOT),
            SIRR=sum(sirr*Area/AREATOT),TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),
            ETR = sum(setr*Area/AREATOT),ETP = sum(setp*Area/AREATOT))
MIRR80 <- METIRR80 %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(YIELD=sum(stemfm*Area/AREATOT),
            SIRR=sum(sirr*Area/AREATOT),TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),
            ETR = sum(setr*Area/AREATOT),ETP = sum(setp*Area/AREATOT))

MREAL <- METREAL %>%
  group_by(YEAR,SCE,SEASON) %>%
  summarise(YIELD=sum(stemfm*Area/AREATOT),YIELDCV=sum(sd(stemfm)/mean(stemfm)*Area/AREATOT),
            SIRR=sum(sirr*Area/AREATOT),TMEAN=sum(stmean/365*Area/AREATOT),
            PPT=sum(spr*Area/AREATOT),SDRAIN=sum(sdrain*Area/AREATOT),PPTCV=sum(sd(spr)/mean(spr)*Area/AREATOT),
            ETR = sum(setr*Area/AREATOT),ETP = sum(setp*Area/AREATOT))

MNOSTRESS<- MNOSTRESS %>% group_by(YEAR,SCE) %>% summarise_all(mean)
MIRR0 <- MIRR0 %>% group_by(YEAR,SCE) %>% summarise_all(mean)
MIRR15 <- MIRR15 %>% group_by(YEAR,SCE) %>% summarise_all(mean)
MIRR35 <- MIRR35 %>% group_by(YEAR,SCE) %>% summarise_all(mean)
MIRR80 <- MIRR80 %>% group_by(YEAR,SCE) %>% summarise_all(mean)
MREAL <- MREAL %>% group_by(YEAR,SCE) %>% summarise_all(mean)

ID1 <- which((MREAL$YEAR %in% 2016:2025)&(MREAL$SCE=='ssp245'))
ID2 <- which((MREAL$YEAR %in% 2056:2065)&(MREAL$SCE=='ssp245'))
ID3 <- which((MREAL$YEAR %in% 2091:2100)&(MREAL$SCE=='ssp245'))
mean(MREAL$TMEAN[ID2])-mean(MREAL$TMEAN[ID1])
(mean(MREAL$ETP[ID2])-mean(MREAL$ETP[ID1]))/mean(MREAL$ETP[ID1])*100
(mean(MREAL$PPT[ID2])-mean(MREAL$PPT[ID1]))/mean(MREAL$PPT[ID1])*100
(mean(MREAL$YIELD[ID2])-mean(MREAL$YIELD[ID1]))/mean(MREAL$YIELD[ID1])*100
(mean(MIRR80$SIRR[ID2])-mean(MIRR80$SIRR[ID1]))/mean(MIRR80$SIRR[ID1])*100
(mean(MIRR80$YIELD[ID2])-mean(MIRR80$YIELD[ID1]))

ID1 <- which((MREAL$YEAR %in% 2016:2025)&(MREAL$SCE=='ssp585'))
ID2 <- which((MREAL$YEAR %in% 2056:2065)&(MREAL$SCE=='ssp585'))
ID3 <- which((MREAL$YEAR %in% 2091:2100)&(MREAL$SCE=='ssp585'))
(mean(MREAL$YIELD[ID2])-mean(MREAL$YIELD[ID1]))/mean(MREAL$YIELD[ID1])*100

#################
P1 <- ggplot(MIRR80, aes(YEAR,TMEAN))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(TMEAN[YEAR%in% 2016:2022]),yend=mean(TMEAN[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Mean Temperature (°C)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  theme(legend.title=element_blank(),legend.position=c(0.2,0.8))
P1

P2 <- ggplot(MIRR80, aes(YEAR,PPT))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(PPT[YEAR%in% 2016:2022]),yend=mean(PPT[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Annual rainfall (mm/yr)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  theme(legend.title=element_blank(),legend.position=c(0.2,0.8))
P2

P3 <- ggplot(MIRR80, aes(YEAR,ETP))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(ETP[YEAR%in% 2016:2022]),yend=mean(ETP[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('ETP (mm/yr)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  theme(legend.title=element_blank(),legend.position=c(0.2,0.8))
P3

library(cowplot)
ggarrange(P1,P2,P3,ncol = 1)#,labels = c('(a)','(b)','(c)'))


P0 <- ggplot(MREAL, aes(YEAR,YIELD))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(YIELD[YEAR%in% 2016:2022]),yend=mean(YIELD[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Sugarcane yield (t/ha)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  annotate(geom = "text",x = 2050,y=140,label="Constant irrigation regime - Reunion Island",size=6)+
  theme(legend.title=element_blank(),legend.position=c(0.1,0.15))
P0
P0 <- ggplot(MIRR15, aes(YEAR,YIELD))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(YIELD[YEAR%in% 2016:2022]),yend=mean(YIELD[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Sugarcane yield (t/ha)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  annotate(geom = "text",x = 2050,y=140,label="Constant irrigation regime - Reunion Island",size=6)+
  theme(legend.title=element_blank(),legend.position=c(0.1,0.15))
P0

P0 <- ggplot(MREAL, aes(YEAR,PPTCV))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(PPTCV[YEAR%in% 2016:2022]),yend=mean(PPTCV[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Sugarcane yield (t/ha)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  #annotate(geom = "text",x = 2050,y=0.45,label="Constant irrigation regime - Reunion Island",size=6)+
  theme(legend.title=element_blank(),legend.position=c(0.1,0.75))
P0

P0 <- ggplot(MIRR80, aes(YEAR,SIRR))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(SIRR[YEAR%in% 2016:2022]),yend=mean(SIRR[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Irrigation demand (mm/yr)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  annotate(geom = "text",x = 2050,y=900,label="Irrigation 80% AWC - Reunion Island",size=6)+
  theme(legend.title=element_blank(),legend.position=c(0.1,0.75))
P0

P0 <- ggplot(MIRR80, aes(YEAR,YIELD))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean(YIELD[YEAR%in% 2016:2022]),yend=mean(YIELD[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab('Sugarcane yield (t/ha)')+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  annotate(geom = "text",x = 2050,y=169,label="Irrigation 80% AWC - Reunion Island",size=6)+
  theme(legend.title=element_blank(),legend.position=c(0.1,0.75))
P0

P0 <- ggplot(MIRR80, aes(YEAR,PPT-ETP))+
  geom_point(shape=21,aes(color=SCE))+
  geom_smooth(aes(color=SCE))+
  geom_segment(aes(y=mean((PPT-ETP)[YEAR%in% 2016:2022]),yend=mean((PPT-ETP)[YEAR%in% 2016:2022])),
               x=2016,xend=2100,linetype=2)+
  xlab('Year')+ylab(expression(delta~(PPT-ETP)~(mm~yr^-1)))+
  theme_classic()+
  scale_color_manual(values = c("#0066CC", "#339900", "#CC0000"),labels=c('SSP1 2.6','SSP2 4.5','SSP5 8.5'))+
  annotate(geom = "text",x = 2050,y=3400,label="Irrigation 80% AWC - Reunion Island",size=6)+
  theme(legend.title=element_blank(),legend.position=c(0.1,0.75))
P0

######## Zone #########
library(raster)
#getData('ISO3')
RUN <- getData('GADM', country='REU', level=0) # Charger la carte
# TRACER L'ECHELLE (attention de régler sa position en longitude, latitude)
# d : distance en km
# xy = c(longitude, latitude)  ==> position de la barre d'échelle
#scalebar(d = 10, xy = c(55.2, -21.1), type = "bar", below = "km",lwd = 4, divs = 2, col = "black", cex = 0.75, lonlat = T)

######### Temperature ##########
library("RColorBrewer")
source('SCRIPT_analyse/R_function_ana.R')

METTEMP <- MATMERGETMEAN(METCHOICE = METIRR0,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in Temperature (°C)')

METTEMP <- MATMERGEETP(METCHOICE = METIRR0,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in ETP (%)')

METTEMP <- MATMERGEPPT(METCHOICE = METIRR0,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in rainfall (%)')

METTEMP <- MATMERGEYIELD(METCHOICE = METREAL,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield (%)',LABIRR='')

METTEMP <- MATMERGEYIELD(METCHOICE = METREAL,zone=ZONE)$MSIM100
PLOTMAP(METTEMP=METTEMP, PERIOD='2091 - 2100', IDSCE=3,RUN=RUN,
        LAB = 'Change in yield (%)',LABIRR='')

METTEMP <- MATMERGEYIELD(METCHOICE = METIRR15,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield (%)',LABIRR='Irrigation 15mm/w')
METTEMP <- MATMERGEYIELD(METCHOICE = METIRR15,zone=ZONE)$MSIM100
PLOTMAP(METTEMP=METTEMP, PERIOD='2091 - 2100', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield (%)',LABIRR='Irrigation 15mm/w')


#### calculs besoin irrigation ####
METTEMP <- MATMERGEIRR(METCHOICE = METIRR80,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in irrigation demand (%)',LABIRR='Irrigation 80% AWC')
METTEMP <- MATMERGEYIELD(METCHOICE = METIRR80,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield (%)',LABIRR='Irrigation 80% AWC')


## calcul
MSIMA$DEL60 <- MSIMB$SIRR-MSIMA$SIRR
MSIMA$DEL100 <- (MSIMC$SIRR-MSIMA$SIRR)
MSIMA <- merge(MSIMA, ZONE[,c('id','Latitude','Longitude')],by='id',all.x=T)
MCUR <- merge(MSIMA, unique(METIRR80[,c("id","Area")]))
head(MCUR)

MCUR$ADD60 <- MCUR$DEL60*MCUR$Area/1000
              
############ yield variability #####
METTEMP <- MATMERGEPPTCV(METCHOICE = METIRR15,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in rainfall CV (%)',LABIRR='')
METTEMP <- MATMERGEPPTCV(METCHOICE = METIRR15,zone=ZONE)$MSIM100
PLOTMAP(METTEMP=METTEMP, PERIOD='2091 - 2100', IDSCE=2,RUN=RUN,
        LAB = 'Change in rainfall CV (%)',LABIRR='')

METTEMP <- MATMERGEYIELDCV(METCHOICE = METREAL,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield CV (%)',LABIRR='')
METTEMP <- MATMERGEYIELDCV(METCHOICE = METREAL,zone=ZONE)$MSIM100
PLOTMAP(METTEMP=METTEMP, PERIOD='2091 - 2100', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield CV (%)',LABIRR='')

METTEMP <- MATMERGEYIELDCV(METCHOICE = METIRR15,zone=ZONE)$MSIM60
PLOTMAP(METTEMP=METTEMP, PERIOD='2056 - 2065', IDSCE=2,RUN=RUN,
        LAB = 'Change in yield CV (%)',LABIRR='Irrigation 15mm/w')

