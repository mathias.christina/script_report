This dataset contains simulated cane yield in 15 different localization in Réunion island from 2002 to 2019.

Input data files are included in the dataset as well as Rdata output datafile

The "CALCULATED_DATA.xlxs" file included simulated cane yield with or without water stress as well as meteorological indices in each localization and year of harvest.