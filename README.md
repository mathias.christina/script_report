R-script of published technical reports
=============================

*Christina Mathias, UPR AIDA, Cirad*

*Contact: mathias.christina@cirad.fr*

*Actualized February 2023*

Licence : <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International</a>.


___

# Description
This project gather the R script used in technical reports published by Christina Mathias from CIRAD (UPR AIDA).

References of the reports:

+ Diagnostic de l'impact du climat sur les rendements de canne à sucre 2022 à La Réunion. Christina, Mathias. 2023. La Réunion : CIRAD. https://rpubs.com/mathiaschristina/1023036

+ Impact du changement climatique sur les rendements canniers dans les 50 prochaines années à La Réunion. Christina Mathias, Le Mézo Lionel, Mézino Mickaël, Todoroff Pierre. 2021. Saint-Denis : CIRAD, 13 p. https://agritrop.cirad.fr/597452/

+ Evolution des rendements canniers potentiels de 2002 à 2019 sur des stations météorologiques de La Réunion. Christina Mathias. 2020. Montpellier : CIRAD, 19 p. https://agritrop.cirad.fr/596837/

